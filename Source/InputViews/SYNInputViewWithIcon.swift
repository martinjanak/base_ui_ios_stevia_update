//
//  SYNInputViewWithIcon.swift
//
//
//  Created by Daniel Rutkovsky on 5/4/17.
//
//

import Stevia
import UIKit

/// View that provides unified layout of an input with icon and bottom line
open class SYNInputViewWithIcon: UIView {

    // MARK: - Views
    public let textField = UITextField()
    private let icon = UIImageView()
    private let bottomLine = UIView()

    // MARK: - Variables
    fileprivate var shouldReturnCallback: (UITextField) -> Bool
    fileprivate var onClickCallback: () -> Void
    public var isEditingEnabled = true

    // MARK: - Init

    /// Creates an UIView With Title, input field and bottom line
    ///
    /// - Parameters:
    ///   - input: parameters of the textField (placeholder, font, color)
    ///   - icon: icon that should be displayed to the left of the input view
    ///   - spacingInputLine: spacing between the input field and bottom line
    ///   - lineColor: color of the bottom line
    ///   - keyboardType: type of keyboard that should be presented. Defaults to `.default`
    ///   - isSecureTextEntry: should the input be secure? Defaults to `false`
    ///   - shouldReturnCallback: callback if the textfield should return -> controlls the keyboard
    public init(input: (placeholder: String, font: UIFont?, color: UIColor),
                icon: UIImage?,
                spacingInputLine: CGFloat = 12,
                lineColor: UIColor = .white,
                keyboardType: UIKeyboardType = .default,
                isSecureTextEntry: Bool = false,
                onClickCallback: @escaping () -> Void = {
        },
                shouldReturnCallback: @escaping (UITextField) -> Bool = { _ in
            return false
        }) {
        self.onClickCallback = onClickCallback
        self.shouldReturnCallback = shouldReturnCallback
        super.init(frame: CGRect.zero)
        setupViews(input: input,
                   icon: icon,
                   lineColor: lineColor,
                   keyboardType: keyboardType,
                   isSecureTextEntry: isSecureTextEntry)
        layoutViews(spacingInputLine: spacingInputLine)
    }

    /// NOT IMPLEMETNED
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Prepare Layout

    private func setupViews(input: (placeholder: String, font: UIFont?, color: UIColor),
                            icon: UIImage?,
                            lineColor: UIColor,
                            keyboardType: UIKeyboardType,
                            isSecureTextEntry: Bool) {
        self.icon.image = icon
        self.icon.tintColor = input.color
        textField.style { txtField in
            txtField.textColor = input.color
            txtField.font = input.font
            txtField.attributedPlaceholder =  NSAttributedString(string: input.placeholder,
                                                                 attributes: [NSForegroundColorAttributeName: lineColor]
            )
            txtField.accessibilityIdentifier = "textField"
            txtField.clearButtonMode = .always
            txtField.tintColor = input.color
        }
        self.icon.accessibilityIdentifier = "icon"
        self.bottomLine.accessibilityIdentifier = "bottomLine"
        textField.delegate = self
        textField.keyboardType = keyboardType
        textField.isSecureTextEntry = isSecureTextEntry
        bottomLine.style { button in
            button.backgroundColor = lineColor
        }
    }

    private func layoutViews(spacingInputLine: CGFloat) {
        sv(icon, textField, bottomLine)
        layout(0,
               |-4 - icon - 12 - textField|,
               spacingInputLine,
               |bottomLine| ~ 1,
               0)
        icon.heightEqualsWidth()
        icon.contentMode = .scaleAspectFit
    }
}

extension SYNInputViewWithIcon: UITextFieldDelegate {

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        onClickCallback()
        return isEditingEnabled
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return shouldReturnCallback(textField)
    }
}
