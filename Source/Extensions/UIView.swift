//
//  UIView.swift
//
//  Created by Filip Bulander on 24/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import UIKit
import Stevia

public extension UIView {
   static func prepareViewWitBottomLine(botomLineColor: UIColor) -> UIView {
        let view = UIView()
        let bottomLine = UIView()
        bottomLine.backgroundColor = botomLineColor
        view.sv(bottomLine)
        bottomLine.left(0).right(0).bottom(0).height(1)
        return view
    }
}
