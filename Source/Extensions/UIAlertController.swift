//
//  UIAlertController.swift
//
//  Created by Filip Bulander on 24/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import UIKit

public extension UIAlertController {
    func setTitleColorAndFont(color: UIColor, font: UIFont) {
        if let title = self.title {
            let attributedString = NSAttributedString(string: title,
                                                      attributes: [NSForegroundColorAttributeName: color,
                                                                   NSFontAttributeName: font])
            self.setValue(attributedString, forKey: "attributedTitle")
        }
    }
}
