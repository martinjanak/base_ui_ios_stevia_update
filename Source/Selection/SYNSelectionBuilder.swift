//
//  SelectionBuilder.swift
//  Expensa
//
//  Created by Dominik on 28/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import Foundation
import SYNBase
import RxSwift
import UIKit

/// Builder class for creation of a selection ViewController
public struct SYNSelectionBuilder {
    public typealias RouterProtocol = SYNSelectionRouterProtocol

    static let kSectionTitle = "kSectionTitle"
    static let kSectionItems = "kSectionItems"

    /// Building method for selection View Controller
    ///
    /// - Parameters:
    ///   - router: Router for selection
    ///   - title: Title of a ViewController
    ///   - items: Items for selection
    ///   - background: Background of whole view
    ///   - selectionColor: Color of selected item
    ///   - cellFont: Font of item views
    ///   - customCell: Custom cell of a selection view
    /// - Returns: Builded SYNSelectionViewController
    static func buildViewController(router: RouterProtocol,
                                    title: String,
                                    items: Observable<[SYNSelectionItem]>,
                                    background: Background = Background.color(color: .white),
                                    selectionColor: UIColor = .gray,
                                    cellFont: UIFont = UIFont.systemFont(ofSize: 15),
                                    customCell: SYNSelectionTableViewCell.Type = SYNSelectionTableViewCell.self)
        -> UIViewController? {
            let presenter = SYNSelectionPresenter(router: router, title: title, array: items)
            let view = SYNSelectionViewController(presenter: presenter,
                                                  background: background,
                                                  selectionColor: selectionColor,
                                                  cellFont: cellFont,
                                                  customCell: customCell)
            presenter.view = view
            return view
    }
}
