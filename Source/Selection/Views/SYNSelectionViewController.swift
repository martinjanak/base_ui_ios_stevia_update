//
//  SelectionViewController.swift
//  AppStore
//
//  Created by Dominik on 28/09/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import RxCocoa
import RxSwift
import Stevia
import UIKit

public protocol SYNSelectionViewProtocol {

}

/// ViewController for selecting one item from an array
public class SYNSelectionViewController<Cell: SYNSelectionTableViewCell>: UIViewController {

    fileprivate var presenter: SYNSelectionPresenterProtocol
    private var background: Background

    private let disposeBag = DisposeBag()

    fileprivate var tableView = { () -> UITableView in
        let view = UITableView()
        view.register(Cell.self,
                           forCellReuseIdentifier: Cell.getIdentifier())
        view.separatorColor = .clear
        view.rowHeight = 64
        view.accessibilityIdentifier = "tableView"
        return view
    } ()

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    /// Constructor for base of selection view controller with modifieable parameters
    ///
    /// - Parameters:
    ///   - presenter: Protocol of type SYNSelectionPresenterProtocol
    ///   - background: Will set the background (color/image) of the VC
    ///   - selectionColor: Will set the color of selected item
    ///   - cellFont: Will set font which will be used for items
    ///   - customCell: Will set custom cell view for selection item
    public init(presenter: SYNSelectionPresenterProtocol,
         background: Background,
         selectionColor: UIColor,
         cellFont: UIFont,
         customCell: Cell.Type) {
        self.presenter = presenter
        self.background = background
        super.init(nibName: nil, bundle: nil)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func setupViews() {
        self.title = presenter.title

        switch self.background {
        case .image(let image):
            let imageBackground = UIImageView(image: image)
            view.sv(imageBackground)
            imageBackground.fillContainer()
        case .color(let color):
            self.view.backgroundColor = color
        }

        view.sv(tableView)
        tableView.fillContainer()
        presenter.array
            .drive(tableView.rx
                .items(cellIdentifier: Cell.getIdentifier(),
                       cellType: Cell.self)) { (_, element, cell) in
                        cell.setImage(image: element.image)
                        cell.setTitle(title: element.title)
            }
            .disposed(by: disposeBag)
        tableView.rx
            .modelSelected(SYNSelectionItem.self)
            .subscribe(onNext: { [weak self] (item) in
                self?.presenter.itemDidSelect(item: item)
            })
        .disposed(by: disposeBag)
    }

    @objc
    fileprivate func backTapped() {
        presenter.didCancel()
    }
}

extension SYNSelectionViewController: SYNSelectionViewProtocol {

}
