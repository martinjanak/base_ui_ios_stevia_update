//
//  BaseProject.h
//  BaseProject
//
//  Created by Daniel Rutkovsky on 3/21/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

@import Foundation;

FOUNDATION_EXPORT double BaseProjectVersionNumber;
FOUNDATION_EXPORT const unsigned char BaseProjectVersionString[];
