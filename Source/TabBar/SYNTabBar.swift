//
//  SYNTabBar.swift
//
//
//  Created by Daniel Rutkovsky on 4/17/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import Stevia
import UIKit

/// SYTabBar
///
/// A viewController that has a tabbar at the bottom of its view.
/// This controller lets you add any number of tabs with thier view controllers.
///
open class SYTabBar: UIViewController {

    /// SYTabBarItem
    ///
    /// Item that represents one tab on the tab bar
    public struct Item {
        public let colors: (active: UIColor, inactive: UIColor)
        public let icons: (active: UIImage?, inactive: UIImage?)
        public let title: String?
        public let font: UIFont?
        public let viewControlerCallback: () -> UIViewController
        public let identifier: String
        fileprivate var index = -1

        /// Init a tab bar item representation
        ///
        /// - Parameters:
        ///   - colors: Color that will be used as tint for image and title
        ///       - active: displayed when tab is active
        ///       - inactive: displayed when tab is inactive
        ///   - icons: Icons thta will be shown on the tab
        ///       - active: displayed when tab is active
        ///       - inactive: displayed when tab is inactive
        ///   - title: Title of the tab
        ///   - viewControler: A function that will provide ViewController that should be presented when tab is selected
        ///   - font: Font that should be used on title _Defaults to_ `UIFont.systemFont(ofSize: 10)`
        ///   - identifier: Identifier of the tab. _Defaults to_ `UUID().uuidString`
        public init(colors: (active: UIColor, inactive: UIColor),
                    icons: (active: UIImage?, inactive: UIImage?),
                    title: String?,
                    viewControler: @escaping () -> UIViewController,
                    font: UIFont? = UIFont.systemFont(ofSize: 10),
                    identifier: String = UUID().uuidString) {
            self.colors = colors
            self.icons = icons
            self.title = title
            self.viewControlerCallback = viewControler
            self.font = font
            self.identifier = identifier
        }
    }

    // MARK: - Property
    fileprivate let presenter: SYNTabBarPresenterProtocol
    fileprivate let kTabBarHeight: CGFloat = 50
    fileprivate var items: [Item] = []
    fileprivate var itemViews: [SYTabBarItemView] = []
    fileprivate var firstItem = 0
    fileprivate var separatorColor: UIColor?
    fileprivate var backgroundColor: UIColor?
    fileprivate var selectedViewController: UIViewController?
    /// Index of the selected item
    public fileprivate(set) var selectedItem = -1
    fileprivate var viewControlerCache: [String: UIViewController] = [:]
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return selectedViewController?.preferredStatusBarStyle ?? .default
    }

    // MARK: - Views
    fileprivate let tabBarView = UIView()
    fileprivate let tabBarSeparatorView = UIView()
    fileprivate let pageView = UIView()

    // MARK: - Prepare Layout
    private func setupViews() {
        view.accessibilityIdentifier = "tabBar"
        automaticallyAdjustsScrollViewInsets = false
        pageView.backgroundColor = .white
        self.view.backgroundColor = backgroundColor
        tabBarView.backgroundColor = backgroundColor
        tabBarSeparatorView.backgroundColor = separatorColor
    }

    private func makeLayout() {
        self.view.sv(pageView, tabBarSeparatorView, tabBarView)
        self.view.layout(0,
                         |pageView|,
                         0,
                         separatorColor == nil ? 0 : (|tabBarSeparatorView| ~ 1),
                         |tabBarView| ~ kTabBarHeight,
                         0)
        self.createTabs()
    }

    // MARK: - Initializer
    /// Init
    ///
    /// - Parameters:
    ///   - presenter: Presenter that controlls this view See: `SYTabBarPresenterProtocol`
    ///   - separatorColor: Color of the separator between viewController and tabbar
    ///   - backgroundColor: Background color of the tab bar _Defaults to_ `UIColor.white`
    ///
    /// - Warning: If you don't provide a `separatorColor` there will be no separator
    ///
    /// - SeeAlso: `SYTabBarPresenterProtocol`
    public init(presenter: SYNTabBarPresenterProtocol,
                separatorColor: UIColor? = nil,
                backgroundColor: UIColor? = .white) {
        self.presenter = presenter
        self.separatorColor = separatorColor
        self.backgroundColor = backgroundColor
        super.init(nibName: nil, bundle: nil)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.makeLayout()
        self.setupViews()
        presenter.loadContent()
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard !items.isEmpty && firstItem < items.count && firstItem >= 0 else {
            return
        }
        if selectedItem >= 0 && selectedItem < items.count {
            presentViewController(item: items[selectedItem])
        } else {
            presentViewController(item: items[firstItem])
        }
    }
}

// MARK: - Private
extension SYTabBar {
    fileprivate func createTabs() {
        var previous: UIView? = nil
        itemViews.forEach { (view) in
            view.removeFromSuperview()
        }
        itemViews.removeAll()
        var index = 0
        for i in 0..<items.count {
            var item = items[i]
            item.index = index
            items[i].index = index
            let view = SYTabBarItemView(colors: item.colors,
                                        icons: item.icons,
                                        title: item.title,
                                        font: item.font,
                                        onClick: { [weak self] in
                                            if let strongSelf = self {
                                                strongSelf.presenter.didClickOnItem(item: item)
                                            }
            })
            view.accessibilityIdentifier = "tabBarItem\(index)"
            tabBarView.sv(view)
            tabBarView.layout(0,
                              view,
                              0)
            if let previous = previous {
                previous - 0 - view
            } else {
                |-0 - view
            }
            itemViews.append(view)
            previous = view
            index += 1
        }
        if let previous = previous {
            previous - 0-|
        }
        equalSizes(itemViews)
    }

    fileprivate func presentViewController(item: Item, pop: Bool = false) {
        if item.index == -1 {
            return
        }
        let controller = getViewController(identifier: item.identifier,
                                           viewControllerInitCallback: item.viewControlerCallback)
        if selectedItem == item.index {
            if let scrollProtocol = controller as? SYNTabBarScrollToTop {
                scrollProtocol.scrollToTop()
            }
            return
        }
        if selectedItem != -1 {
            itemViews[self.selectedItem].setActive(active: false)
        }
        self.selectedItem = item.index
        itemViews[self.selectedItem].setActive(active: true)
        if let nc = controller as? UINavigationController, pop {
            nc.popToRootViewController(animated: false)
        }
        pageView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        selectedViewController?.removeFromParentViewController()
        addChildViewController(controller)
        pageView.sv(controller.view)
        pageView.layout(0,
                        |controller.view|,
                        0)
        controller.didMove(toParentViewController: self)
        selectedViewController = controller
        self.setNeedsStatusBarAppearanceUpdate()
    }

    fileprivate func getViewController(identifier: String,
                                       viewControllerInitCallback: @escaping () -> UIViewController) -> UIViewController {
        return viewControlerCache[identifier] ?? { () -> UIViewController in
            let vc = viewControllerInitCallback()
            self.viewControlerCache[identifier] = vc
            return vc
            }()
    }
}

// MARK: - SYTabBarViewInterface
extension SYTabBar: SYNTabBarViewProtocol {

    public func presentItems(items: [SYTabBar.Item], selectedIndex: Int = 0) {
        self.items.removeAll()
        self.items.append(contentsOf: items)
        self.firstItem = selectedIndex
        createTabs()
    }

    public func addItem(item: SYTabBar.Item) {
        items.append(item)
        createTabs()
    }

    public func selectItem(item: SYTabBar.Item) {
        selectItem(identifier: item.identifier)
    }

    public func selectItem(identifier: String) {
        for item in items where item.identifier == identifier {
            self.presentViewController(item: item)
        }
    }
}

fileprivate class SYTabBarItemView: UIView {
    private let activeIcon = UIImageView()
    private let inactiveIcon = UIImageView()
    private let colors: (active: UIColor, inactive: UIColor)
    private let title = UILabel()
    private var callback        : () -> Void

    required init(colors: (active: UIColor, inactive: UIColor),
                  icons: (active: UIImage?, inactive: UIImage?),
                  title: String?,
                  font: UIFont?,
                  onClick: @escaping () -> Void) {
        self.activeIcon.image = icons.active
        self.inactiveIcon.image = icons.inactive
        self.title.text = title
        self.callback = onClick
        self.colors = colors
        super.init(frame: CGRect.zero)
        self.makeLayout()
        self.setupViews(font: font, colors: colors)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupViews(font: UIFont?, colors: (active: UIColor, inactive: UIColor)) {
        self.backgroundColor = .clear
        title.font = font
        title.textAlignment = .center
        title.setContentCompressionResistancePriority(900, for: .vertical)
        inactiveIcon.tintColor = colors.inactive
        activeIcon.tintColor = colors.active
        title.textColor = colors.inactive
        title.highlightedTextColor = colors.active
        title.adjustsFontSizeToFitWidth = true

        self.activeIcon.isHidden = true
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                         action: #selector(onTabClicked)))
    }

    private func makeLayout() {
        sv(activeIcon, inactiveIcon, title)
        let borderPadding: CGFloat = 8
        layout(borderPadding,
               activeIcon)
        layout(|-borderPadding - title - borderPadding-|,
               borderPadding)
        if title.text != nil {
            addConstraint(item: activeIcon,
                          attribute: .bottom,
                          relatedBy: .equal,
                          toItem: title,
                          attribute: .top,
                          multiplier: 1.0,
                          constant: -borderPadding / 2)
        } else {
            addConstraint(item: activeIcon,
                          attribute: .bottom,
                          relatedBy: .equal,
                          toItem: self,
                          attribute: .bottom,
                          multiplier: 1.0,
                          constant: -borderPadding)
        }
        activeIcon.keepAspectRatio()
        activeIcon.centerHorizontally()
        activeIcon.left(>=4)
        inactiveIcon.keepAspectRatio()
        equalSizes(activeIcon, inactiveIcon)
        alignCenter(activeIcon, with: inactiveIcon)
    }

    public func setActive(active: Bool) {
        self.activeIcon.isHidden = !active
        self.inactiveIcon.isHidden = active
        self.title.isHighlighted = active
    }

    internal func onTabClicked() {
        self.callback()
    }
}
