//
//  SYTabBarView-PresenterProtocols.swift
//  
//
//  Created by Daniel Rutkovsky on 4/17/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import SYNBase

/// SYTabBarViewProtocol
///
/// A protocol that abstracts communication between SYTabBar and SYTabBarPresenter
public protocol SYNTabBarViewProtocol: class {
    /// Present items on the tab bar
    ///
    /// - Warning: All previous tabs will be removed. To add just one tab use `addItem(item:)`
    /// - Parameters:
    ///   - items: Items to be displayed
    ///   - selectedIndex: Index of the item that will be selected _Defaults to_ `0`
    func presentItems(items: [SYTabBar.Item], selectedIndex: Int)
    /// Add item to current list of items
    ///
    /// - Parameter item: Item to be added
    func addItem(item: SYTabBar.Item)
    /// Select item on the tabbar and present its viewController
    ///
    /// - Parameter item: Item to be selected
    /// - Precondition: Item with existing `identifier` must be presented on tabbar
    func selectItem(item: SYTabBar.Item)
    /// Select item on the tabbar and present its viewController<#Description#>
    ///
    /// - Parameter identifier: Identifier of the item to be selected
    func selectItem(identifier: String)
}

/// SYTabBarPresenterProtocol
///
/// A protocol that abstracts communication between SYTabBar and SYTabBarPresenter
public protocol SYNTabBarPresenterProtocol: BasePresenterProtocol {
    /// A reference to the view
    weak var view: SYNTabBarViewProtocol? { get }
    /// A _callback_ that will be called whenever the user tabs one of tabbar's items
    ///
    /// - Warning: The tab bar doesn't select automatically tapped items. 
    ///            Use `selectItem(item: SYTabBar.Item)` to select it.
    /// - Parameter item: Item that was tapped
    func didClickOnItem(item: SYTabBar.Item)
    /// A _callback_ that will be called whenever the item has changed
    ///
    /// - Parameter item: Item that is now dispalyed
    func didShowItem(item: SYTabBar.Item)
}

/// A default implemntation of `didClickOnItem(item:)`
public extension SYNTabBarPresenterProtocol {
    /// Select item that was clicked on
    func didClickOnItem(item: SYTabBar.Item) {
        view?.selectItem(item: item)
    }
}

/// A protocol for scrolling table view by clicking already selected tab
public protocol SYNTabBarScrollToTop {
    func scrollToTop()
}
