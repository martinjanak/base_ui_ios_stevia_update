//
//  UIImageView+Extensions.swift
//  Pods
//
//  Created by Daniel Rutkovsky on 4/18/17.
//
//

import Foundation

extension UIImageView {
    /// Keeps aspect ratio of the image -> height equals width
    func keepAspectRatio() {
        keepAspectRatioHeightBasedOnWidth()
    }

    /// Keeps aspect ratio of the image -> height equals width
    func keepAspectRatioHeightBasedOnWidth() {
        if let image = self.image {
            let ratio: CGFloat = image.size.height / image.size.width
            self.addConstraint(NSLayoutConstraint(item: self,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: .width,
                                                  multiplier: ratio,
                                                  constant: 0)
            )
        }
    }

    /// Keeps aspect ratio of the image -> width equals height
    func keepAspectRatioWidthBasedOnHeight() {
        if let image = self.image {
            let ratio: CGFloat = image.size.width / image.size.height
            self.addConstraint(NSLayoutConstraint(item: self,
                                                  attribute: .width,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: .height,
                                                  multiplier: ratio,
                                                  constant: 0)
            )
        }
    }
}
