//
//  SYNNavigationBarView.swift
//  
//
//  Created by Daniel Rutkovsky on 6/20/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import Stevia
import UIKit

open class SYNNavigationBarView: UIView {
    // MARK: - Variables
    public weak var delegate: SYNNavigationBarDelegate?

    // MARK: - Views
    fileprivate let leftItem = UIView()
    fileprivate let rightItem = UIView()
    internal let lblTitle = { () -> UILabel in
        let label = UILabel()
        label.minimumScaleFactor = 0.5
        label.accessibilityIdentifier = "title"
        return label
    } ()

    // MARK: - Prepare Layout
    private func setupViews(left: UIView?,
                            right: UIView?,
                            color: UIColor,
                            title: (text: String, font: UIFont?, color: UIColor?)) {
        backgroundColor = color

        lblTitle.text = title.text
        lblTitle.font = title.font ?? UIFont.boldSystemFont(ofSize: 16)
        lblTitle.textColor = title.color ?? UIColor.black

        leftItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onLeftItemTaped)))
        leftItem.isUserInteractionEnabled = true
        rightItem.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onRightItemTaped)))
        rightItem.isUserInteractionEnabled = true
        lblTitle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTitleTaped)))
        lblTitle.isUserInteractionEnabled = true

        if let left = left {
            leftItem.sv(left)
            left.fillContainer()
        }

        if let right = right {
            rightItem.sv(right)
            right.fillContainer()
        }

        accessibilityIdentifier = "SYNNavigationBarView"
        leftItem.accessibilityIdentifier = "leftItem"
        rightItem.accessibilityIdentifier = "rightItem"
    }

    private func layoutViews() {
        sv(leftItem, lblTitle, rightItem)
        layout(0,
               |leftItem - >=8 - lblTitle - >=8 - rightItem|,
               0)
        alignHorizontally(leftItem, lblTitle, rightItem)
        lblTitle.centerInContainer()
        leftItem.heightEqualsWidth()
        equalSizes(leftItem, rightItem)
        leftItem.setContentHuggingPriority(900, for: .horizontal)
        leftItem.setContentCompressionResistancePriority(900, for: .horizontal)
        rightItem.setContentHuggingPriority(900, for: .horizontal)
        rightItem.setContentCompressionResistancePriority(900, for: .horizontal)
    }

    // MARK: - Initializer
    /// Init the navigation bar with images
    ///
    /// - Parameters:
    ///   - title: A title to display `(text: String, font: UIFont, color: UIColor)`
    ///   - leftItem: An image to be displayed on the left
    ///   - rightItem: An image to be displayed on the right
    ///   - backgroundColor: Background color of the navigation bar. Defaults to `UIColor.white`
    ///   - delegate: A delegate to listen for callbacks. Defaults to `nil`
    public convenience init(title: (text: String, font: UIFont?, color: UIColor?),
                            leftItem: UIImage?,
                            rightItem: UIImage?,
                            backgroundColor: UIColor = UIColor.white,
                            imageTintColor: UIColor? = nil) {
        let left = SYNNavigationBarView.convertUIImageIntoUIView(leftItem, imageTintColor: imageTintColor)
        let right = SYNNavigationBarView.convertUIImageIntoUIView(rightItem, imageTintColor: imageTintColor)
        self.init(title: title,
                  leftItem: left,
                  rightItem: right,
                  backgroundColor: backgroundColor)
    }

    /// Init the navigation bar with custom views
    ///
    /// - Parameters:
    ///   - title: A title to display `(text: String, font: UIFont, color: UIColor)`
    ///   - leftItem: A custom view to be displayed on the left. Defaults to `nil`
    ///   - rightItem: A custom view to be displayed on the right. Defaults to `nil`
    ///   - backgroundColor: Background color of the navigation bar. Defaults to `UIColor.white`
    ///   - delegate: A delegate to listen for callbacks. Defaults to `nil`
    public init(title: (text: String, font: UIFont?, color: UIColor?),
                leftItem: UIView? = nil,
                rightItem: UIView? = nil,
                backgroundColor: UIColor = UIColor.white) {
        super.init(frame: .zero)
        layoutViews()
        setupViews(left: leftItem, right: rightItem, color: backgroundColor, title: title)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SYNNavigationBarView {
    @objc
    fileprivate func onLeftItemTaped() {
        delegate?.didClickOnLeftItem()
    }

    @objc
    fileprivate func onRightItemTaped() {
        delegate?.didClickOnRightItem()
    }

    @objc
    fileprivate func onTitleTaped() {
        delegate?.didClickOnTitle()
    }

    internal static func convertUIImageIntoUIView(_ image: UIImage?, imageTintColor: UIColor? = nil) -> UIView {
        let imageView = UIImageView(image: image)
        let view = UIView()
        view.sv(imageView)
        imageView.left(>=4).top(>=4).right(>=4).bottom(>=4)
        imageView.centerInContainer()
        imageView.contentMode = .scaleAspectFit
        if let imageTintColor = imageTintColor {
            imageView.tintColor = imageTintColor
        }
        return view
    }
}
