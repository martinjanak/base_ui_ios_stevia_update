//
//  SYNNavigationBar.swift
//
//
//  Created by Daniel Rutkovsky on 5/2/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import Stevia
import UIKit


public protocol HasSYNNavigationBar {
    var navigationBar: SYNNavigationBar { get }
}

/// SYNNavigationBar
///
/// An impelemntation of a basic navigation bar.
/// It provides the developer to dispalay two custom views on each side of the bar
/// and title in the middle.
/// It will trigger callbacks when user taps any of the views -> See `SYNNavigationBarDelegate`
/// - WARN: It will cover the space underneath the statsu bar => _*IT HAS TO BE POSITIONED ON THE TOP OF THE SCREEN*_
///
open class SYNNavigationBar: UIView {
    // MARK: - Variables
    public weak var delegate: SYNNavigationBarDelegate? {
        get {
            return navigationBar.delegate
        }
        set {
            navigationBar.delegate = newValue
        }
    }

    // MARK: - Views
    fileprivate let navigationBar: SYNNavigationBarView
    private let statusBarBackground = UIView()
    fileprivate var progressBar: UIView?
    fileprivate var progressBarProgress: Double?

    // MARK: - Prepare Layout
    private func setupViews(_ color: UIColor) {
        backgroundColor = color
        statusBarBackground.backgroundColor = color
        accessibilityIdentifier = "SYNNavigationBar"
    }

    private func layoutViews() {
        sv(statusBarBackground, navigationBar)
        layout(0,
               |statusBarBackground| ~ 20,
               |navigationBar|,
               0)
    }

    // MARK: - Initializer
    /// Init the navigation bar with images
    ///
    /// - Parameters:
    ///   - title: A title to display `(text: String, font: UIFont, color: UIColor)`
    ///   - leftItem: An image to be displayed on the left
    ///   - rightItem: An image to be displayed on the right
    ///   - backgroundColor: Background color of the navigation bar. Defaults to `UIColor.white`
    ///   - delegate: A delegate to listen for callbacks. Defaults to `nil`
    public convenience init(title: (text: String, font: UIFont?, color: UIColor?),
                            leftItem: UIImage?,
                            rightItem: UIImage?,
                            backgroundColor: UIColor = UIColor.white,
                            imageTintColor: UIColor? = nil) {
        let left = SYNNavigationBarView.convertUIImageIntoUIView(leftItem, imageTintColor: imageTintColor)
        let right = SYNNavigationBarView.convertUIImageIntoUIView(rightItem, imageTintColor: imageTintColor)
        self.init(title: title,
                  leftItem: left,
                  rightItem: right,
                  backgroundColor: backgroundColor)
    }

    /// Init the navigation bar with custom views
    ///
    /// - Parameters:
    ///   - title: A title to display `(text: String, font: UIFont, color: UIColor)`
    ///   - leftItem: A custom view to be displayed on the left. Defaults to `nil`
    ///   - rightItem: A custom view to be displayed on the right. Defaults to `nil`
    ///   - backgroundColor: Background color of the navigation bar. Defaults to `UIColor.white`
    ///   - delegate: A delegate to listen for callbacks. Defaults to `nil`
    public init(title: (text: String, font: UIFont?, color: UIColor?),
                leftItem: UIView? = nil,
                rightItem: UIView? = nil,
                backgroundColor: UIColor = UIColor.white) {
        navigationBar = SYNNavigationBarView(title: title,
                                             leftItem: leftItem,
                                             rightItem: rightItem,
                                             backgroundColor: backgroundColor)
        super.init(frame: .zero)
        layoutViews()
        setupViews(backgroundColor)
    }

    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func setTitle(title: String) {
        navigationBar.lblTitle.text = title
    }

}


extension SYNNavigationBar: SYNProgressBarProtocol {
    public func setupProgressBar(backgroundImage: UIImage?, initialProgress: Double) {
        let progressBar = UIView()
        let imageView = prepareImage(image: backgroundImage)
        sv(progressBar.sv(imageView))
        progressBar.layout(0,
                           |imageView,
                           0)
        |progressBar| ~ 10
        addConstraint(item: navigationBar,
                      attribute: .bottom,
                      relatedBy: .equal,
                      toItem: progressBar,
                      attribute: .centerY,
                      multiplier: 1.0,
                      constant: 0)
        self.progressBar = progressBar
        setProgress(progress: initialProgress, animate: false)
    }

    private func prepareImage(image: UIImage?) -> UIImageView {
        let view = UIImageView(image: image)
        view.contentMode = .scaleToFill
        return view
    }

    public func setProgress(progress: Double, animate: Bool) {
        progressBarProgress = progress
        if let view = self.progressBar?.subviews.first {
            let setBlock = {
                let width = self.frame.size.width * CGFloat(progress)
                if let constraint = view.widthConstraint {
                    constraint.constant = width
                } else {
                    view.width(width)
                }
                view.superview?.layoutIfNeeded()
            }
            if animate {
                UIView.animate(withDuration: 0.4, animations: setBlock)
            } else {
                setBlock()
            }
        }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        if let progress = progressBarProgress {
            setProgress(progress: progress, animate: false)
        }
    }
}


