//
//  SYNNavigationBarDelegate.swift
//
//
//  Created by Daniel Rutkovsky on 5/2/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import Foundation

/// SYNNavigationBarDelegate
/// 
/// A delegate for reacting on actions of the navigation bar
/// Default implementation does nothing when the actions are recieved
///
public protocol SYNNavigationBarDelegate: class {
    /// User did select the item on the left side of the bar
    func didClickOnLeftItem()
    /// User did select the item on the right side of the bar
    func didClickOnRightItem()
    /// User did tap on the title
    func didClickOnTitle()
}

public extension SYNNavigationBarDelegate {
    func didClickOnLeftItem() {
    }
    func didClickOnRightItem() {
    }
    func didClickOnTitle() {
    }
}
